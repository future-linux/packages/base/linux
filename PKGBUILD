# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=(linux linux-headers linux-docs)
pkgbase=linux
pkgver=5.18.10
pkgrel=1
arch=('x86_64')
url="https://www.kernel.org/"
license=('GPL2')
groups=('base')
depends=()
makedepends=('bash' 'bc' 'binutils' 'coreutils' 'diffutils' 'findutils' 'gcc' 'glibc'
    'grep' 'gzip' 'kmod' 'libelf' 'make' 'ncurses' 'openssl' 'perl' 'sed') # 'pahole')
options=('!strip')
source=(https://www.kernel.org/pub/linux/kernel/v5.x/${pkgbase}-${pkgver}.tar.xz
    config-${pkgver})
sha256sums=(f99e3d01dbb8ee6fe765d9cb2b2e073afd2dc581c0d3979751c862745d73dee3
    42f00264894945dcdd3edc9eaa945e6d5acc0db142dbcd8238f3b715baf6ca9e)

export KBUILD_BUILD_HOST=futurelinux
export KBUILD_BUILD_USER=${pkgbase}
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

prepare() {
    cd ${pkgbase}-${pkgver}

    echo "Setting version..."
    scripts/setlocalversion --save-scmversion
    # echo "-${pkgrel}" > localversion.10-pkgrel
    echo "${pkgbase#linux}" > localversion.20-pkgname

    echo "Setting config..."
    cp ${srcdir}/config-${pkgver} .config
    make olddefconfig
    diff -u cp ${srcdir}/config-${pkgver} .config || :

    make -s kernelrelease > version
    echo "Prepared ${pkgbase} version $(<version)"
}

build() {
    cd ${pkgbase}-${pkgver}

    make bzImage modules
    make -C tools/objtool
}

package_linux() {
    pkgdesc="The linux kernel and modules"
    depends=('coreutils' 'linux-firmware' 'kmod' 'dracut')

    cd ${pkgbase}-${pkgver}

    local kernver=$(<version)
    local modulesdir=${pkgdir}/usr/lib/modules/${kernver}

    echo "Installing boot image..."
    # systemd expects to find the kernel here to allow hibernation
    # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
    install -Dm644 "$(make -s image_name)" ${modulesdir}/vmlinuz

    # Used by mkinitcpio to name the kernel
    echo ${pkgbase} | install -Dm644 /dev/stdin ${modulesdir}/pkgbase

    echo "Installing modules..."
    make INSTALL_MOD_PATH=${pkgdir}/usr modules_install

    # remove build and source links
    rm ${modulesdir}/{source,build}
}

package_linux-headers() {
    pkgdesc="Header files and scripts for building modules for linux kernel"
    # depends=('pahole')

    cd ${pkgbase}-${pkgver}

    local builddir=${pkgdir}/usr/lib/modules/$(<version)/build

    echo "Installing build files..."
    install -Dt ${builddir} -m644 .config Makefile Module.symvers System.map vmlinux
    install -Dt ${builddir}/kernel -m644 kernel/Makefile
    install -Dt ${builddir}/arch/x86 -m644 arch/x86/Makefile
    cp -t ${builddir} -a scripts

    # required when STACK_VALIDATION is enabled
    install -Dt ${builddir}/tools/objtool tools/objtool/objtool

    # required when DEBUG_INFO_BTF_MODULES is enabled
    # install -Dt ${builddir}/tools/bpf/resolve_btfids tools/bpf/resolve_btfids/resolve_btfids

    cp -t ${builddir} -a include
    cp -t ${builddir}/arch/x86 -a arch/x86/include
    install -Dt ${builddir}/arch/x86/kernel -m644 arch/x86/kernel/asm-offsets.s

    install -Dt ${builddir}/drivers/md -m644 drivers/md/*.h
    install -Dt ${builddir}/net/mac80211 -m644 net/mac80211/*.h

    # https://bugs.archlinux.org/task/13146
    install -Dt ${builddir}/drivers/media/i2c -m644 drivers/media/i2c/msp3400-driver.h

    # https://bugs.archlinux.org/task/20402
    install -Dt ${builddir}/drivers/media/usb/dvb-usb -m644 drivers/media/usb/dvb-usb/*.h
    install -Dt ${builddir}/drivers/media/dvb-frontends -m644 drivers/media/dvb-frontends/*.h
    install -Dt ${builddir}/drivers/media/tuners -m644 drivers/media/tuners/*.h

    # https://bugs.archlinux.org/task/71392
    install -Dt ${builddir}/drivers/iio/common/hid-sensors -m644 drivers/iio/common/hid-sensors/*.h

    echo "Installing KConfig files..."
    find . -name 'Kconfig*' -exec install -Dm644 {} "${builddir}/{}" \;

    echo "Removing unneeded architectures..."
    local arch
    for arch in ${builddir}/arch/*/; do
        [[ ${arch} = */x86/ ]] && continue
        echo "Removing $(basename ${arch})"
        rm -r ${arch}
    done

    echo "Removing documentation..."
    rm -r ${builddir}/Documentation

    echo "Removing broken symlinks..."
    find -L ${builddir} -type l -printf 'Removing %P\n' -delete

    echo "Removing loose objects..."
    find ${builddir} -type f -name '*.o' -printf 'Removing %P\n' -delete

    echo "Stripping build tools..."
    local file
    while read -rd '' file; do
        case "$(file -bi ${file})" in
            application/x-sharedlib\;*)      # Libraries (.so)
                strip ${STRIP_SHARED} ${file} ;;
            application/x-archive\;*)        # Libraries (.a)
                strip ${STRIP_STATIC} ${file} ;;
            application/x-executable\;*)     # Binaries
                strip ${STRIP_BINARIES} ${file} ;;
            application/x-pie-executable\;*) # Relocatable binaries
                strip ${STRIP_SHARED} ${file} ;;
        esac
    done < <(find ${builddir} -type f -perm -u+x ! -name vmlinux -print0)

    echo "Stripping vmlinux..."
    strip -v ${STRIP_STATIC} ${builddir}/vmlinux

    echo "Adding symlink..."
    install -vdm755 ${pkgdir}/usr/src
    ln -sr ${builddir} ${pkgdir}/usr/src/${pkgbase}
}

package_linux-docs() {
    pkgdesc="Documentation for the linux kernel"

    cd ${pkgbase}-${pkgver}

    local builddir=${pkgdir}/usr/lib/modules/$(<version)/build

    echo "Installing documentation..."
    local src dst
    while read -rd '' src; do
        dst=${src#Documentation/}
        dst=${builddir}/Documentation/${dst#output/}
        install -Dm644 ${src} ${dst}
    done < <(find Documentation -name '.*' -prune -o ! -type d -print0)

    echo "Adding symlink..."
    install -vdm755 ${pkgdir}/usr/share/doc
    ln -sr ${builddir}/Documentation ${pkgdir}/usr/share/doc/${pkgbase}
}
